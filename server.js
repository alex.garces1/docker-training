const express = require('express');
const path = require('path');
const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const app = express();
const fetch = require('node-fetch');
require('dotenv').config();

let MONGO_CLIENT_OPTIONS = { useNewUrlParser: true, useUnifiedTopology: true };
let DATABASE_NAME = "user-account";

let mongoUrl = null;

if (process.env.DB_HOST) {
  mongoUrl = 'mongodb://' + process.env.DB_USERNAME + ':' + process.env.DB_PASSWORD + '@' + process.env.DB_HOST;
} else {
  console.log('Host not provided. Running stateless mode.')
}

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get('/profile-picture', async (req, res) => {
  if (process.env.PROFILE_IMAGE) {
    try {
      const response = await fetch(process.env.PROFILE_IMAGE);
      const imageBuffer = await response.buffer();
      res.set('Content-Type', 'image/jpeg');
      return res.send(imageBuffer);
    } catch (error) {
      console.error('Error fetching custom profile image:', error);
    }
  }

  let img = fs.readFileSync(path.join(__dirname, "images/profile-1.jpg"));
  res.writeHead(200, { 'Content-Type': 'image/jpg' });
  res.end(img, 'binary');
});

app.post('/update-profile', function (req, res) {
  let userObj = req.body;

  if (!mongoUrl) return res.send(userObj);

  MongoClient.connect(mongoUrl, MONGO_CLIENT_OPTIONS, function (err, client) {
    if (err) throw err;
    let db = client.db(DATABASE_NAME);
    userObj['userid'] = 1;

    let myquery = { userid: 1 };
    let newvalues = { $set: userObj };

    db.collection("users").updateOne(myquery, newvalues, { upsert: true }, function (err, res) {
      if (err) throw err;
      client.close();
    });

    console.log('User profile updated successfully!', { userObj })
  });
  // Send response
  res.send(userObj);
});

app.get('/get-profile', function (req, res) {
  let response = {};

  if (!mongoUrl) return res.send(response);

  // Connect to the db
  MongoClient.connect(mongoUrl, MONGO_CLIENT_OPTIONS, function (err, client) {
    if (err) throw err;
    let db = client.db(DATABASE_NAME);

    let myquery = { userid: 1 };

    db.collection("users").findOne(myquery, function (err, result) {
      if (err) throw err;
      response = result;
      client.close();

      // Send response
      res.send(response || {});
    });
  });
});

app.listen(3000, function () {
  console.log("app listening on port 3000!");
});
