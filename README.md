# Docker Training

## Demo App

This repository sets up a Node.js demo app that shows a simple user profile using:

- index.html with pure js and css styles
- nodejs backend with express server

To launch it:

- Run `npm install`
- Run `node server.js`
- Open `http://localhost:3000/` in your browser.

## Challenge 1

Run the Node.js application of the repo (root folder) **inside a Docker container**.

- It should be accessible via browser at `http://localhost:3000/`
- You should add a PROFILE_IMAGE environment variable **in the Docker command, not in an .env file**. It should contain the URL of an online image.

**You can use only one command to solve it. You don't need to change any code of the repo to make it work.**

## Challenge 2

Add persistence to the application.

### Set up the network

- Create a network

### First container: MongoDB database

- It should run in the previously created network
- It should have a name (eg. `mongodb`)
- Bind the container and your computer to the same port (use the default MongoDB port)
- You shoud set the `admin` username and `password` password via environment variables in the command. No other environment variables are needed.

### Second container: Node.js app

- It should run in the previously created network
- It should be accessible via browser at `http://localhost:3000/`
- It should have the following environment variables:
  - DB_HOST={the name of the database container}
  - DB_USERNAME=admin
  - DB_PASSWORD=password
  - PROFILE_IMAGE (Optional)

**You can use only three commands to solve it. You don't need to change any code of the repo to make it work.**

For it to run properly, you need to create the containers in the specified order.

## Challenge 3

Set up the Challenge 2 application with Docker Compose instead of commands :smile:

## Commands Overview

### Basic Commands

Pull and image from Dockerhub

    docker pull redis

Check all images on my computer

    docker images

Create and run a container of an image

    docker run redis

Pull image and create container directly

    docker run redis:7.2-alpine

Check all running containers

    docker ps

Check all containers

    docker ps -a

Run container in detached mode

    docker run -d redis

Add a specific name to a container

    docker run -d --name my-redis redis

Stop container

    docker stop f6c83ca8717c

Start previously stopped container

    docker start my-redis

Remove a container (only when it's stopped!)

    docker rm f6c83ca8717c

Stop all containers

    docker stop $(docker ps -aq)

Remove all containers

    docker rm $(docker ps -aq)

Bind host port to container port (for it to be reachable)

    docker run -d -p 6000:6379 redis

Bind host volume (file system) to container

    docker run -d -v $(pwd)/images:/usr/src redis

### Debugging Commands

Run command line inside a container (see files, env. vars, etc.)

    docker exec -it b551743043bc /bin/bash
    docker exec -it my-redis sh

Show container logs

    docker logs 435993df2c8d
    docker logs my-redis

### Networking Commands

Check all networks in my computer

    docker network ls

Check all networks in my computer

    docker network create alex-network

Run a container in a specific network

    docker run -d redis --network alex-network

## Credits

This training is largely based on [Nana Janashia`s Docker Tutorial for Beginners](https://www.youtube.com/watch?v=3c-iBn73dDE&ab_channel=TechWorldwithNana), which I highly recommend!
